import socket
import json
import re
import os

if os.path.exists('mem_list.txt'):
    f = open("mem_list.txt", "w")
    f.write("{}")
    f.close()
    pass
else:
    f = open("mem_list.txt", "x")
    f.close()


def join_node():
    try:
        os.system('python3 runtime.py &')
    except:
        return 'Failed to join!'
    return 'Joined successfully!'

def leave_node():
    stream1 = os.popen('ps -ef | grep runtime.py | grep -v grep')
    stream2 = os.popen('ps -ef | grep detect.py | grep -v grep')
    res1 = stream1.read()
    res2 = stream2.read()
    if not res1 and not res2:
        return 'This node is not running!'
    
    if res1:
        pid1 = res1.split()[1]
        print('Killing', pid1)
        try:
            res = os.system('kill -9 '+pid1)
        except:
            res = 1
    
    if res2:
        pid2 = res2.split()[1]
        print('Killing', pid2)
        try:
            res = os.system('kill -9 '+pid2)
        except:
            res = 1

    if res == 0:
        return 'Killed successfully!'
    else:
        return 'Failed to kill!'

def check_status():
    pass

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 1024000)
# Bind to any IP assigned to this machine and port 7777
s.bind(('', 7777))

while True: # received_str = ["Join 1", (1.1.1.1, 1234)]
    # command_str = input('Enter command: ')
    received_str = s.recvfrom(1024)
    command_str = received_str[0].decode()
    address = received_str[1]
    command = command_str.split()[0]
    node_num = 0
    try:
        node_num = command_str.split()[1]
    except:
        pass
    if command == 'list_mem':
        f = open("mem_list.txt", "r")
        all_text = f.read()
        if all_text == '':
            info = {}
        else:
            info = json.loads(json.dumps(all_text))
        info = json.dumps(info)
        f.close()
    elif command == 'list_self':
        stream = os.popen('hostname')
        str0 = stream.read()
        try:
            node_num = re.findall(r'fa22-cs425-44(\d\d)',str0)[0]
        except:
            node_num = 'Not found'
        info = 'My ID is: ' + node_num
    elif command == 'join':
        info = join_node()
    elif command == 'leave':
        info = leave_node()
    else:
        print('Command not recognized!')
        continue

    # print(info)
    s.sendto(str.encode(info), address)
