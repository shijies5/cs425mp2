class MyNode:
    node_num = ""
    monitor = ""
    introducer = ""
    isDown = True
    def __init__(self,node_num,monitor,introducer,isDown):
        self.node_num = node_num
        self.monitor=monitor
        self.introducer=introducer
        self.isDown=isDown

    def __repr__(self) -> str:
        if self.isDown == True:
            pres= "Server is down!!!"
        else:
            pres="Server is running."
        return f"node_num = {self.node_num}, monitor = {self.monitor}, introducer = {self.introducer}, isDown = {pres}"
    def toString(self) ->str:
        if(self.isDown == True):
            pres = "1"
        else:
            pres = "0"
        return self.node_num+" "+self.monitor+" "+self.introducer+" "+pres+"\n"