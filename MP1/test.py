import client

print("Testing 'GET'")
assert client.log_query("GET") == ['170372', '160563', '161169', '162590', '162714', '161189', '161050', '164776', '161852', '159130'], "'GET' test failed."

print("Testing '532.19.3'")
assert client.log_query("532.19.3") == ['34', '22', '34', '32', '26', '40', '22', '40', '30', '30'], "'532.19.3' test failed."

print("Testing regular expression '2\d{2}.\d{3}.\d{3}.\d{3}'")
assert client.log_query("2\d{2}.\d{3}.\d{3}.\d{3}") == ['13978', '13368', '13176', '13504', '13272', '13410', '13308', '13836', '13567', '13217'], "RE test failed."
