# Client receive logs from server
import os
import socket
import re
import time

def log_query(pattern):
    # Set the server list
    server_list = [('fa22-cs425-4401.cs.illinois.edu',6666),('fa22-cs425-4402.cs.illinois.edu',6666),('fa22-cs425-4403.cs.illinois.edu',6666),('fa22-cs425-4404.cs.illinois.edu',6666),('fa22-cs425-4405.cs.illinois.edu',6666),('fa22-cs425-4406.cs.illinois.edu',6666),('fa22-cs425-4407.cs.illinois.edu',6666),('fa22-cs425-4408.cs.illinois.edu',6666),('fa22-cs425-4409.cs.illinois.edu',6666),('fa22-cs425-4410.cs.illinois.edu',6666)]
    i = 1 # index of server_list
    ts0 = time.time()
    num_pattern = []
    for server in server_list:
    # Set the socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect(server)
        except:
            print("Server",i,"is down.")
            i += 1
            continue
    
    # Send the request and receive the response
        s.send(pattern.encode())
        num = s.recv(1024).decode()
        num_pattern += [num]
        print('vm',i,':',num)

        s.close()
        #print('Received', file_name, 'from', server)
        i += 1
    ts1 = time.time()
    print('Time:',ts1-ts0)
    return num_pattern
