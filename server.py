# Server send log
import socket
import re
import os
from typing import ByteString

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 1024000)
# Bind to any IP assigned to this machine and port 6666
msgFromServer       = "Hello UDP Client"

bytesToSend         = str.encode(msgFromServer)

s.bind(('', 6666))
print("UDP server up and listening")
# s.listen()
# stream = os.popen('hostname')
# str0 = stream.read()
# i = int(re.findall(r'fa22-cs425-44(\d\d)',str0)[0])
while True:

    bytesAddressPair = s.recvfrom(1024)

    message = bytesAddressPair[0]
    address = bytesAddressPair[1]

    clientMsg = "Message from Client:{}".format(message)
    clientIP  = "Client IP Address:{}".format(address)
    
    print(clientMsg)
    print(clientIP)
    # Send a reply to the client
    s.sendto(bytesToSend, address)

    # conn, addr = s.accept()
    # print('Connected by', addr)
    
    # Receive the pattern
    # pattern = conn.recv(1024).decode()
    # print('Received pattern:', pattern)
    # f = open('/home/shijies5/cs425mp1/MP1/vm'+str(i)+'.log', 'r')
    # str_all = f.read()
 
    # result = re.findall(pattern, str_all)
    # #str(len(result)).encode()
    # conn.send(str(len(result)).encode())
    
    # f.close()
    # conn.close()
