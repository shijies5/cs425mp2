import socket
import json
import time
import os
import re
import ast
import utils

f = open("mem_list.txt", "r")
mem_dict = {}
all_text = f.read()
if all_text == '':
    pass
else:
    mem_dict = ast.literal_eval(json.loads(json.dumps(all_text)))
f.close()

UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
server_list = [('fa22-cs425-4401.cs.illinois.edu',7777),('fa22-cs425-4402.cs.illinois.edu',7777),('fa22-cs425-4403.cs.illinois.edu',7777),('fa22-cs425-4404.cs.illinois.edu',7777),('fa22-cs425-4405.cs.illinois.edu',7777),('fa22-cs425-4406.cs.illinois.edu',7777),('fa22-cs425-4407.cs.illinois.edu',7777),('fa22-cs425-4408.cs.illinois.edu',7777),('fa22-cs425-4409.cs.illinois.edu',7777),('fa22-cs425-4410.cs.illinois.edu',7777)]
#server_list = ("127.0.0.1", 7777)

while True:
    command_str = input('Enter command: ')
    command = command_str.split()[0]
    node_num = 0
    try:
        node_num = int(command_str.split()[1])
    except:
        pass

    msgFromServer = ''
    if command == 'list_self':
        stream = os.popen('hostname')
        str0 = stream.read()
        try:
            node_num = re.findall(r'fa22-cs425-44(\d\d)',str0)[0]
        except:
            node_num = 'Not found'
        print('My ID is: ' + node_num)

    elif command == 'list_mem' or command == 'join' or command == 'leave':
        UDPClientSocket.sendto(str.encode(command), server_list[int(node_num)-1])
        UDPClientSocket.settimeout(1)
        try:
            msgFromServer = UDPClientSocket.recvfrom(1024)
            print(msgFromServer[0].decode())
        except:
            print('Command not completed!')
    elif command == 'quit':
        break    
    else:
        print('Command not recognized!')

    # print(msgFromServer)
    if command == 'join' and msgFromServer:
        ts = time.time()
        address = msgFromServer[1][0]
        node_id = str(node_num)+"_"+str(ts)+"_"+str(address)
        res1 = utils.updateList(command, node_num, node_id, mem_dict)

        sendFullListFirst = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        
        full_string = 'Full '+str(mem_dict)
        print(full_string)
        time.sleep(2)
        for i in range(1, int(node_num)):
            sendFullListFirst.sendto(str.encode(full_string), (server_list[i][0],9999))
        # print((server_list[int(node_num)-1][0],9999))
        res2 = utils.gossipUpdate(command, node_num, 1, mem_dict)
        if res1 == 1 or res2 == 1:
            pass
            # LOGGING
    elif command == 'leave' and msgFromServer:
        res = utils.updateList(command, node_num, None, mem_dict)
        if res == 1:
            pass
            # LOGGING
