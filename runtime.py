import os
import re
import ast
import time
import json
import math
import socket
import utils

# Start the detect.py process
try:
    os.system('python3 detect.py >> detect.log &')
except:
    pass

f = open("mem_list.txt", "r")
mem_dict = {}
all_text = f.read()
if all_text == '':
    pass
else:
    mem_dict = ast.literal_eval(json.loads(json.dumps(all_text)))
f.close()

# Get my node_num
stream = os.popen('hostname')
str0 = stream.read()
node_num = int(re.findall(r'fa22-cs425-44(\d\d)',str0)[0])

server_list = [('fa22-cs425-4401.cs.illinois.edu',9999),('fa22-cs425-4402.cs.illinois.edu',9999),('fa22-cs425-4403.cs.illinois.edu',9999),('fa22-cs425-4404.cs.illinois.edu',9999),('fa22-cs425-4405.cs.illinois.edu',9999),('fa22-cs425-4406.cs.illinois.edu',9999),('fa22-cs425-4407.cs.illinois.edu',9999),('fa22-cs425-4408.cs.illinois.edu',9999),('fa22-cs425-4409.cs.illinois.edu',9999),('fa22-cs425-4410.cs.illinois.edu',9999)]


while True:
    f = open("mem_list.txt", "r")
    all_text = f.read()
    if all_text == '':
        mem_dict = {}
    else:
        mem_dict = ast.literal_eval(json.loads(json.dumps(all_text)))
    f.close()
    
    isMonitor = 0
    nodes = len(list(mem_dict.keys()))

    for key in mem_dict.keys():
        if key.split('_')[0] == str(node_num):
            isMonitor = mem_dict[key][0]
            break
    if not isMonitor:
        continue
    if nodes < 5:
        continue

    nodes = len(mem_dict.keys())
    shift = math.ceil(nodes*3/8)

    for i in range(node_num-shift, node_num+shift+1):
        if i != node_num:
            utils.detect(server_list[int(i-1)], i, mem_dict)
        else:
            pass
    
    time.sleep(3)
