import socket
import json
import ast
import random

def detect(server, node_num, mem_dict):
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    UDPClientSocket.sendto(str.encode('ping'), server)
    UDPClientSocket.settimeout(1)
    try:
        msgFromServer = UDPClientSocket.recvfrom(1024)
    except:
        updateList('leave', node_num, None, mem_dict)
        gossipUpdate('leave', node_num, 1, mem_dict)
        return 1
    return 0

# Update the local mem_list.txt
def updateList(oper, node_num, node_id, mem_dict={}):
    # global mem_dict
    print("**************")
    print ("oper : ", oper)
    print("mem_dict : ",mem_dict)
    print("**************")
    
    # Add 0 ahead of 1-digit node_num
    if int(node_num) < 10:
        node_num_str = str(node_num)
    else:
        node_num_str = '0'+str(node_num)

    #Change mem_dict type to dict if full_string
    if type(mem_dict) != dict:
        mem_dict = ast.literal_eval(mem_dict)

    if oper == 'join':
        mem_dict[node_id] = [0,0]
    elif oper == 'leave':
        for key in mem_dict.keys():
            if key.split('_')[0] == node_num_str:
                print(1)
                del mem_dict[key]
                print(mem_dict)
                break
            else:
                pass

    # Update monitor status
    for i in range(len(list(mem_dict.keys()))):
        if i < 4:
            mem_dict[list(mem_dict.keys())[int(i)]][0] = 1
        else:
            mem_dict[list(mem_dict.keys())[int(i)]][0] = 0
        
    f = open("mem_list.txt", "w")
    f.write(json.dumps(mem_dict))
    f.close()
    
    return 0

def gossipUpdate(oper, node_num, iter_num, mem_dict):
    server_list = [('fa22-cs425-4401.cs.illinois.edu',9999),('fa22-cs425-4402.cs.illinois.edu',9999),('fa22-cs425-4403.cs.illinois.edu',9999),('fa22-cs425-4404.cs.illinois.edu',9999),('fa22-cs425-4405.cs.illinois.edu',9999),('fa22-cs425-4406.cs.illinois.edu',9999),('fa22-cs425-4407.cs.illinois.edu',9999),('fa22-cs425-4408.cs.illinois.edu',9999),('fa22-cs425-4409.cs.illinois.edu',9999),('fa22-cs425-4410.cs.illinois.edu',9999)]

    if int(iter_num) > 5:
        return 0
    
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    # UDPClientSocket.settimeout(1)
    nodes = len(list(mem_dict.keys()))
    for i in range (3):
        node = random.randint(0, nodes-1)
        recv_node_num = list(mem_dict.keys())[int(node)].split('_')[0]
        UDPClientSocket.sendto(str.encode(oper+' '+str(node_num)+' '+str(iter_num+1)), (server_list[int(recv_node_num)][0],9999))
    
